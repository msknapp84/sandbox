#!/bin/bash

# check that maven is installed
if [ $(mvn --version | wc -l) < 1 ]; then
	# install maven
	
	# TODO add method to do that.

fi

PROJECT_NAME=$1
sudo mkdir $PROJECT_NAME
sudo mkdir $PROJECT_NAME/src
sudo mkdir $PROJECT_NAME/src/main
sudo mkdir $PROJECT_NAME/src/main/java
sudo mkdir $PROJECT_NAME/src/main/python
sudo mkdir $PROJECT_NAME/src/main/resources
sudo mkdir $PROJECT_NAME/src/test
sudo mkdir $PROJECT_NAME/src/test/java
sudo mkdir $PROJECT_NAME/src/test/python
sudo mkdir $PROJECT_NAME/src/test/resources
sudo mkdir $PROJECT_NAME/src/main/webapp
sudo mkdir $PROJECT_NAME/src/main/filters
sudo mkdir $PROJECT_NAME/src/main/avro
sudo mkdir $PROJECT_NAME/logs
sudo mkdir $PROJECT_NAME/target
sudo mkdir $PROJECT_NAME/tmp
sudo mkdir $PROJECT_NAME/scripts
sudo cp ~/templates/log4j.xml $PROJECT_NAME/log4j.xml
sudo cp ~/templates/pom.xml $PROJECT_NAME/pom.xml

# GIT?
cd $PROJECT_NAME
git init
