package foo;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class BarReducer extends MapReduceBase implements Reducer<Text,LongWritable,Text,LongWritable> {

	@Override
	public void reduce(Text arg0, Iterator<LongWritable> arg1,
			OutputCollector<Text, LongWritable> arg2, Reporter arg3)
			throws IOException {
		// need to aggregate a sum
		long total = 0L;
		while (arg1.hasNext()) {
			total += arg1.next().get();
		}
		arg2.collect(arg0, new LongWritable(total));
	}
	
}