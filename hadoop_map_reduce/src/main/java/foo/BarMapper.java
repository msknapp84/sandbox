package foo;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import au.com.bytecode.opencsv.CSVParser;

public class BarMapper extends MapReduceBase implements
		org.apache.hadoop.mapred.Mapper<LongWritable, Text, Text, LongWritable> {

	CSVParser parser = new CSVParser();

	@Override
	public void map(LongWritable arg0, Text line,
			OutputCollector<Text, LongWritable> collector, Reporter arg3)
			throws IOException {
		try {
//			System.out.println("Evaluating "+line.toString());
			KV kv = parseLine(line.toString());
			if (kv != null && kv.state!=null && !kv.state.isEmpty()) {
				collector.collect(new Text(kv.state), new LongWritable(kv.value));
//				System.out.println("Wrote "+kv.state+" = "+kv.value);
			}
		} catch (Exception e) {
			System.out.println(e.getClass()+e.getMessage());
		}
	}

	public KV parseLine(String line) {
		// AL,Alabama,-,All U.S.
		// Destinations,15,Coal," 1,718 "," 26,869 "," 4,063 ",21.8,19.2,29.9
		try {
			if (line != null && !line.toString().isEmpty()) {
				String[] parts = parser.parseLine(line.toString());
				String commodity = parts[5];
				if ("Machinery".equals(commodity)) {
					String state = parts[0];
					String value = parts[6];
					Long val = Long.valueOf(value.replaceAll(",", "").trim());
					return new KV(state,val);
				}
			}
		} catch (IOException e) {
			System.out.println(e.getClass()+e.getMessage());
		} catch (Exception e) {
			System.out.println(e.getClass()+e.getMessage());
		}
		return null;
	}
	public static class KV {
		public String state;
		public long value;
		public KV(String state,long value) {
			this.state = state;
			this.value = value;
		}
	}
}