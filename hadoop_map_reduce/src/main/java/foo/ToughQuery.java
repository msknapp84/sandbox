package foo;

import java.io.IOException;
import java.nio.charset.CharacterCodingException;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.RawComparator;
import org.apache.hadoop.io.ShortWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Partitioner;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVParser;

public class ToughQuery extends Configured implements Tool {
	private static final Logger logger = Logger.getLogger(ToughQuery.class);

	public static void main(String[] args) throws Exception {
		System.exit(ToolRunner.run(new Configuration(), new ToughQuery(), args));
	}

	@Override
	public int run(String[] arguments) throws Exception {
		Path in = new Path(
				"/user/cloudera/commodity/Origin_by_Destination_by_Commodity.csv");
		Path out = new Path("/user/cloudera/commodity/" + arguments[0]);
		Configuration conf = getConf();
		JobConf job = new JobConf(conf, ToughQuery.class);
		job.setMapperClass(FirstMapper.class);
		job.setReducerClass(FirstReducer.class);
//		job.setCombinerClass(FirstReducer.class);
		job.setInputFormat(TextInputFormat.class);
		job.setOutputFormat(TextOutputFormat.class);
		FileInputFormat.setInputPaths(job, in);
		FileOutputFormat.setOutputPath(job, out);
		job.setJobName("toughquery");
		job.setPartitionerClass(FirstPartitioner.class);
		job.setOutputValueGroupingComparator(FirstGroupComparator.class);
		job.setNumReduceTasks(5);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(ShortWritable.class);
		job.setOutputValueClass(Text.class);
		JobClient.runJob(job);
		return 0;
	}

	/**
	 * First mapper will filter out "All Commodities" lines. The key is a
	 * combination of commodity number and state. The value is a tab delimited
	 * string, starting with the commodity, then the state, and followed by all
	 * the values from the input line.
	 * 
	 * @author cloudera
	 * 
	 */
	public static final class FirstMapper extends MapReduceBase implements
			Mapper<LongWritable, Text, Text, Text> {
		private CSVParser parser = new CSVParser();

		@Override
		public void map(LongWritable key, Text value,
				OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {
			try {
				String[] ret = coremap(value.toString());
				if (ret != null) {
					output.collect(new Text(ret[0]), new Text(ret[1]));
				}
			} catch (Exception e) {
				logger.error(e);
			}
		}

		public String[] coremap(String value) {
			String[] ret = null;
			try {
				String[] line = parser.parseLine(value.toString());
				// AL,Alabama,-,All U.S. Destinations,00,All
				// Commodities," 182,785 "," 268,926 "," 58,222 ",9.1,9.8,11.2
				String commodity = line[5];
				String commodityNumber = line[4];
				if (!"All Commodities".equals(commodity)) {
					String state = line[0];
					// since the key has both commodity number and state,
					// and the partitionaler and grouping comparator are based
					// solely on the commodity number,
					// I have effectively created a secondary sort.
					String key = commodityNumber + "\t" + state;
					String[] vals = new String[6];
					System.arraycopy(line, 6, vals, 0,
							Math.min(line.length - 6, vals.length));
					for (int i = 0; i < vals.length; i++) {
						vals[i] = vals[i].trim().replaceAll(",", "");
					}
					String val = commodity + "\t" + state + "\t"
							+ StringUtils.join(vals, "\t");
					ret = new String[] { key, val };
				}
			} catch (Exception e) {
				logger.error(e);
			}
			return ret;
		}
	}

	/**
	 * Partitions based on the commodity number alone, and we are creating a
	 * total order partition so everything is ordered.
	 * 
	 * @author cloudera
	 * 
	 */
	public static class FirstPartitioner implements Partitioner<Text, Text> {

		@Override
		public int getPartition(Text key, Text value, int numPartitions) {
			// partition by the state only.
			String[] parts = key.toString().split("\t");
			String commodityNumber = parts[0];
			int num = Integer.parseInt(commodityNumber);
			// approximately 50 values.
			int max = 50;
			int sz = (int) Math.ceil(max / numPartitions);
			for (int i = 0; i < numPartitions; i++) {
				int start = i * sz;
				int end = (i + 1) * sz;
				if (num >= start && num < end) {
					return i;
				}
			}
			return numPartitions - 1;
		}

		@Override
		public void configure(JobConf job) {

		}
	}

	/**
	 * This will group them based on commodity number alone.
	 * 
	 * @author cloudera
	 * 
	 */
	public static class FirstGroupComparator implements RawComparator<Text> {

		@Override
		public int compare(Text o1, Text o2) {
			return compare(o1.toString(), o2.toString());
		}

		public int compare(String val1, String val2) {
			// the values are tab delimited, first part is the
			// commodity number
			String c1 = val1.split("\t")[0];
			String c2 = val2.split("\t")[0];
			long l1 = Long.valueOf(c1);
			long l2 = Long.valueOf(c2);
			// we want ascending order
			return (int) (l1 - l2);
		}

		@Override
		public int compare(byte[] b1, int i1, int s1, byte[] b2, int i2, int s2) {
			try {
				String val1 = Text.decode(b1, i1, s1);
				// byte[] v1 = new byte[s1];
				// System.arraycopy(b1, i1, v1, 0, s1);
				// String val1 = new String(v1);
				System.out.println("val1=" + val1);

				String val2;
				val2 = Text.decode(b2, i2, s2);
				// byte[] v2 = new byte[s2];
				// System.arraycopy(b2, i2, v2, 0, s2);
				// String val2 = new String(v2);
				System.out.println("val2=" + val2);
				// the values are tab delimited, first part is the
				// commodity number
				return compare(val1, val2);
			} catch (CharacterCodingException e) {
				e.printStackTrace();
			}
			return 0;
		}
	}

	/**
	 * Aggregates the sum of all values
	 * 
	 * @author cloudera
	 * 
	 */
	public static class FirstReducer extends MapReduceBase implements
			Reducer<Text, Text, ShortWritable, Text> {

		@Override
		public void reduce(Text key, Iterator<Text> values,
				OutputCollector<ShortWritable, Text> output, Reporter reporter)
				throws IOException {
			try {
				coreReduce(key, values, output);
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
			}
		}

		public void coreReduce(Text key, Iterator<Text> values,
				OutputCollector<ShortWritable, Text> output) throws IOException {
			long[] sums = new long[] { 0L, 0L, 0L };
			double[] dsums = new double[] { 0, 0, 0 };
			String lastState = null;
			String commodity = "";
			// values are ordered by state due to the secondary sort,
			// I can count on that so I can avoid caching large objects
			// in memory.
			while (values.hasNext()) {
				try {
					String state = "";
					String val = values.next().toString();
					System.out.println("From std out, val="+val);
					logger.info("From logger, val="+val);
					if (val == null || val.isEmpty()) {
						continue;
					}
					String[] splits = val.split("\t");
					if (splits.length != 8) {
						logger.warn("split length != 8");
						continue;
					}
					// the commodity and state will be the same value for all of
					// them.
					commodity = splits[0];
					state = splits[1];
					if (lastState != null && !state.equals(lastState)) {
						// the state has changed.
						// commodity is always the same.
						// time to record it.
						record(key, commodity, lastState, sums, dsums, output);
						// restart counts.
						sums = new long[] { 0L, 0L, 0L };
						dsums = new double[] { 0, 0, 0 };
					}
					lastState = state;

					for (int i = 2; i < 8; i++) {
						String cval = splits[i];
						if ("S".equals(cval) || "-".equals(cval) || "Z".equals(cval) || cval.isEmpty()) {
							continue;
						}
						if (i < 5) {
							String lval = cval.replace(",", "").trim();
							try {
								Long r = Long.parseLong(lval);
								sums[i - 2] += r;
							} catch (Exception e) {
								logger.error(e.getMessage());
							}
						} else {
							try {
								double dval = Double.parseDouble(cval.trim());
								dsums[i - 5] += dval;
							} catch (Exception e) {
								logger.error(e.getMessage());
							}
						}
					}
				} catch (Exception e) {
					logger.error(e.getMessage(),e);
				}
			}
			// the last state should also be recorded.
			record(key, commodity, lastState, sums, dsums, output);
		}

		private void record(Text key, String commodity, String lastState,
				long[] sums, double[] dsums,
				OutputCollector<ShortWritable, Text> output) throws IOException {
			// the key will have a commodity number and state
			// the state needs to be tossed
			String realKey = key.toString().split("\t")[0];
			short s = Short.parseShort(realKey);
			String fval = String.format("%s\t%s\t%d\t%d\t%d\t%.1f\t%.1f\t%.1f",
					commodity, lastState, sums[0], sums[1], sums[2], dsums[0],
					dsums[1], dsums[2]);
			logger.info("Recording:"+s+"\t"+fval);
			output.collect(new ShortWritable(s), new Text(fval));
		}
	}
}