package foo;

import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.RawComparator;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class Bar extends Configured implements Tool {

	@Override
	public int run(String[] arguments) throws Exception {
		Options options = new Options();
		options.addOption("f", true, "path to the file in hdfs to process.");
		options.addOption("o", true, "output directory path ");
		PosixParser commandLineParser = new PosixParser();
		try {
			CommandLine commandLine = commandLineParser.parse(options, arguments);
			
			Configuration conf = getConf();
			JobConf job = new JobConf(conf,Bar.class);
			
			Path in = new Path(commandLine.getOptionValue('f'));
			Path tmp = new Path("/tmp/foo-"+System.currentTimeMillis());
			Path out = new Path(commandLine.getOptionValue('o'));
			System.out.println("input path is "+in);
			System.out.println("intermediate tmp path is "+out);
			System.out.println("output path is "+out);
			
			FileInputFormat.setInputPaths(job,in);
			FileOutputFormat.setOutputPath(job, tmp);
			job.setJobName("foo");
			job.setInputFormat(TextInputFormat.class);
			job.setOutputFormat(TextOutputFormat.class);
			job.setMapOutputKeyClass(Text.class);
			job.setMapOutputValueClass(LongWritable.class);
			job.setCombinerClass(BarReducer.class);
			job.setNumReduceTasks(1);
			job.setOutputKeyClass(Text.class);
			job.setOutputValueClass(LongWritable.class);
			job.setMapperClass(BarMapper.class);
			job.setReducerClass(BarReducer.class);
			JobClient.runJob(job);
			
			System.out.println("Sorting descending.");
			
			JobConf job2 = new JobConf(conf,Bar.class);
			job2.setJobName("sorting-foo");
			job2.setInputFormat(TextInputFormat.class);
			job2.setOutputFormat(TextOutputFormat.class);
			job2.setMapOutputKeyClass(LongWritable.class);
			job2.setMapOutputValueClass(Text.class);
			job2.setOutputKeyClass(Text.class);
			job2.setOutputValueClass(LongWritable.class);
			job2.setNumReduceTasks(1);
			job2.setMapperClass(MyMapper.class);
			job2.setReducerClass(MyReducer.class);
			FileInputFormat.setInputPaths(job2, tmp);
			FileOutputFormat.setOutputPath(job2, out);
			job2.setOutputKeyComparatorClass(MyDescendingSort.class);
			JobClient.runJob(job2);
			
		} catch (ParseException e) {
			HelpFormatter hf = new HelpFormatter();
			String usage="Do my thing";
			hf.printHelp(usage, options);
		}
		return 0;
	}
	
	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new Configuration(), new Bar(), args);
		System.exit(exitCode);
	}
	
	private static final class MyMapper extends MapReduceBase implements Mapper<LongWritable,Text,LongWritable,Text> {

		@Override
		public void map(LongWritable key, Text value,
				OutputCollector<LongWritable, Text> output, Reporter reporter)
				throws IOException {
			try {
				String[] kv = value.toString().split("\\s+");
				String state = kv[0];
				String val = kv[1];
				Long v = Long.parseLong(val);
				output.collect(new LongWritable(v), new Text(state));
			} catch (Exception e) {
				System.out.println(e.getClass()+" "+e.getMessage());
			}
		}
	}
	
	private static final class MyReducer extends MapReduceBase implements Reducer<LongWritable,Text,Text,LongWritable> {

		@Override
		public void reduce(LongWritable key, Iterator<Text> values,
				OutputCollector<Text, LongWritable> output, Reporter reporter)
				throws IOException {
			try {
				while (values.hasNext()) {
					output.collect(values.next(),key);
				}
			} catch (Exception e) {
				System.out.println(e.getClass()+" "+e.getMessage());
			}
		}
	}
	
	private static final class MyDescendingSort implements RawComparator<LongWritable> {
		
		public MyDescendingSort() {
			
		}
		
		@Override
		public int compare(byte[] b1,int s1, int l1, byte[] b2, int s2,int l2) {
			return -WritableComparator.compareBytes(b1, s1, l1, b2, s2, l2);
		}

		@Override
		public int compare(LongWritable o1, LongWritable o2) {
			return -(o1.compareTo(o2));
		}
	}
}