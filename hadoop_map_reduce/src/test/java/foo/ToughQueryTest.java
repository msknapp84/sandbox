package foo;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.hadoop.io.ShortWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.OutputCollector;
import org.junit.Assert;
import org.junit.Test;

public class ToughQueryTest {
	
	private String sample = "AL,Alabama,-,All U.S. Destinations,11,Natural sands, S ,\" 3,550 \", S ,S,35.2,S";
	
	@Test
	public void map() {
		ToughQuery.FirstMapper m = new ToughQuery.FirstMapper();
		String[] ret = m.coremap(sample);
		Assert.assertEquals("11\tAL", ret[0]);
		Assert.assertEquals("Natural sands\tAL\tS\t3550\tS\tS\t35.2\tS", ret[1]);
	}
	
	@Test
	public void reduce() throws IOException {
		ToughQuery.FirstReducer r = new ToughQuery.FirstReducer();
		Text key = new Text("11\tAl");
		List<Text> ts=Arrays.asList(
				new Text("Natural sands\tAL\t8\t1000\tS\t52\t10.2\tS"),
				new Text("Natural sands\tAL\tS\t2000\tS\t52\t10.2\tS"),
				new Text("Natural sands\tMD\t-\t1500\t2\t3\t80.1\tS"),
				new Text("Natural sands\tMD\t3\t1800\t2\t3\t80.1\tS"),
				new Text("Natural sands\tNY\t8\t100\t9\tS\t19.5\t8.3"),
				new Text("Natural sands\tNY\t23\t1600\t9\tS\t19.5\t8.3"),
				new Text("Natural sands\tWY\t3\t1600\t9\tS\t19.5\t8.3")
				);
		MyOutputCollector c = new MyOutputCollector();
		r.reduce(key, ts.iterator(),c,null);
		List<KV> ret = c.retreived;
		Assert.assertEquals((short)11, ret.get(0).k);
		Assert.assertEquals((short)11, ret.get(1).k);
		Assert.assertEquals((short)11, ret.get(2).k);
		Assert.assertEquals((short)11, ret.get(3).k);
		Assert.assertEquals("Natural sands\tAL\t8\t3000\t0\t104.0\t20.4\t0.0", ret.get(0).v);
		Assert.assertEquals("Natural sands\tMD\t3\t3300\t4\t6.0\t160.2\t0.0", ret.get(1).v);
		Assert.assertEquals("Natural sands\tNY\t31\t1700\t18\t0.0\t39.0\t16.6", ret.get(2).v);
		Assert.assertEquals("Natural sands\tWY\t3\t1600\t9\t0.0\t19.5\t8.3", ret.get(3).v);
	}
	
	@Test
	public void partition() {
		ToughQuery.FirstPartitioner p = new ToughQuery.FirstPartitioner();
		Assert.assertEquals(0,p.getPartition(new Text("9\tAL"), new Text(), 5));
		Assert.assertEquals(1,p.getPartition(new Text("10\tAL"), new Text(), 5));
		Assert.assertEquals(1,p.getPartition(new Text("19\tNY"), new Text(), 5));
		Assert.assertEquals(2,p.getPartition(new Text("20\tNY"), new Text(), 5));
		Assert.assertEquals(4,p.getPartition(new Text("50\tNY"), new Text(), 5));
		Assert.assertEquals(4,p.getPartition(new Text("90\tWY"), new Text(), 5));
	}
	@Test
	public void group() {
		ToughQuery.FirstGroupComparator c = new ToughQuery.FirstGroupComparator();
		Assert.assertEquals(0,c.compare("11\tAL", "11\tNY"));
		Assert.assertEquals(-3,c.compare("8\tAL", "11\tNY"));
		Assert.assertEquals(5,c.compare("8\tAL", "3\tNY"));
	}
	
	private static class MyOutputCollector implements OutputCollector<ShortWritable,Text> {
		List<KV> retreived = new ArrayList<KV>();
		@Override
		public void collect(ShortWritable key, Text value) throws IOException {
			KV kv = new KV();
			kv.k=key.get();
			kv.v = value.toString();
			retreived.add(kv);
		}
		
	}
	
	private static class KV {
		short k;
		String v;
	}
}
