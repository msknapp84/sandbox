package foo;

import org.junit.Assert;
import org.junit.Test;

import foo.BarMapper.KV;

public class BarMapperTest {
	
	@Test
	public void map() {
		String line = "AL,Alabama,-,All U.S. Destinations,15,Machinery,\" 1,718 \",\" 26,869 \",\" 4,063 \",21.8,19.2,29.9";
		KV os = new BarMapper().parseLine(line);
		Assert.assertEquals("AL",os.state);
		Assert.assertEquals(1718L,os.value);
	}
	// AL,Alabama,-,All U.S. Destinations,15,Coal," 1,718 "," 26,869 "," 4,063 ",21.8,19.2,29.9
}