// ORM class for table 'commodity'
// WARNING: This class is AUTO-GENERATED. Modify at your own risk.
//
// Debug information:
// Generated date: Wed Jan 15 21:25:43 PST 2014
// For connector: org.apache.sqoop.manager.MySQLManager
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;
import com.cloudera.sqoop.lib.JdbcWritableBridge;
import com.cloudera.sqoop.lib.DelimiterSet;
import com.cloudera.sqoop.lib.FieldFormatter;
import com.cloudera.sqoop.lib.RecordParser;
import com.cloudera.sqoop.lib.BooleanParser;
import com.cloudera.sqoop.lib.BlobRef;
import com.cloudera.sqoop.lib.ClobRef;
import com.cloudera.sqoop.lib.LargeObjectLoader;
import com.cloudera.sqoop.lib.SqoopRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class commodity extends SqoopRecord  implements DBWritable, Writable {
  private final int PROTOCOL_VERSION = 3;
  public int getClassFormatVersion() { return PROTOCOL_VERSION; }
  protected ResultSet __cur_result_set;
  private String state;
  public String get_state() {
    return state;
  }
  public void set_state(String state) {
    this.state = state;
  }
  public commodity with_state(String state) {
    this.state = state;
    return this;
  }
  private String origin;
  public String get_origin() {
    return origin;
  }
  public void set_origin(String origin) {
    this.origin = origin;
  }
  public commodity with_origin(String origin) {
    this.origin = origin;
    return this;
  }
  private String destination_state;
  public String get_destination_state() {
    return destination_state;
  }
  public void set_destination_state(String destination_state) {
    this.destination_state = destination_state;
  }
  public commodity with_destination_state(String destination_state) {
    this.destination_state = destination_state;
    return this;
  }
  private String destination;
  public String get_destination() {
    return destination;
  }
  public void set_destination(String destination) {
    this.destination = destination;
  }
  public commodity with_destination(String destination) {
    this.destination = destination;
    return this;
  }
  private Integer sctg;
  public Integer get_sctg() {
    return sctg;
  }
  public void set_sctg(Integer sctg) {
    this.sctg = sctg;
  }
  public commodity with_sctg(Integer sctg) {
    this.sctg = sctg;
    return this;
  }
  private String commodity;
  public String get_commodity() {
    return commodity;
  }
  public void set_commodity(String commodity) {
    this.commodity = commodity;
  }
  public commodity with_commodity(String commodity) {
    this.commodity = commodity;
    return this;
  }
  private Integer value;
  public Integer get_value() {
    return value;
  }
  public void set_value(Integer value) {
    this.value = value;
  }
  public commodity with_value(Integer value) {
    this.value = value;
    return this;
  }
  private Integer tons;
  public Integer get_tons() {
    return tons;
  }
  public void set_tons(Integer tons) {
    this.tons = tons;
  }
  public commodity with_tons(Integer tons) {
    this.tons = tons;
    return this;
  }
  private Integer ton_miles;
  public Integer get_ton_miles() {
    return ton_miles;
  }
  public void set_ton_miles(Integer ton_miles) {
    this.ton_miles = ton_miles;
  }
  public commodity with_ton_miles(Integer ton_miles) {
    this.ton_miles = ton_miles;
    return this;
  }
  private Double value_cv;
  public Double get_value_cv() {
    return value_cv;
  }
  public void set_value_cv(Double value_cv) {
    this.value_cv = value_cv;
  }
  public commodity with_value_cv(Double value_cv) {
    this.value_cv = value_cv;
    return this;
  }
  private Double tons_cv;
  public Double get_tons_cv() {
    return tons_cv;
  }
  public void set_tons_cv(Double tons_cv) {
    this.tons_cv = tons_cv;
  }
  public commodity with_tons_cv(Double tons_cv) {
    this.tons_cv = tons_cv;
    return this;
  }
  private Double ton_miles_cv;
  public Double get_ton_miles_cv() {
    return ton_miles_cv;
  }
  public void set_ton_miles_cv(Double ton_miles_cv) {
    this.ton_miles_cv = ton_miles_cv;
  }
  public commodity with_ton_miles_cv(Double ton_miles_cv) {
    this.ton_miles_cv = ton_miles_cv;
    return this;
  }
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof commodity)) {
      return false;
    }
    commodity that = (commodity) o;
    boolean equal = true;
    equal = equal && (this.state == null ? that.state == null : this.state.equals(that.state));
    equal = equal && (this.origin == null ? that.origin == null : this.origin.equals(that.origin));
    equal = equal && (this.destination_state == null ? that.destination_state == null : this.destination_state.equals(that.destination_state));
    equal = equal && (this.destination == null ? that.destination == null : this.destination.equals(that.destination));
    equal = equal && (this.sctg == null ? that.sctg == null : this.sctg.equals(that.sctg));
    equal = equal && (this.commodity == null ? that.commodity == null : this.commodity.equals(that.commodity));
    equal = equal && (this.value == null ? that.value == null : this.value.equals(that.value));
    equal = equal && (this.tons == null ? that.tons == null : this.tons.equals(that.tons));
    equal = equal && (this.ton_miles == null ? that.ton_miles == null : this.ton_miles.equals(that.ton_miles));
    equal = equal && (this.value_cv == null ? that.value_cv == null : this.value_cv.equals(that.value_cv));
    equal = equal && (this.tons_cv == null ? that.tons_cv == null : this.tons_cv.equals(that.tons_cv));
    equal = equal && (this.ton_miles_cv == null ? that.ton_miles_cv == null : this.ton_miles_cv.equals(that.ton_miles_cv));
    return equal;
  }
  public void readFields(ResultSet __dbResults) throws SQLException {
    this.__cur_result_set = __dbResults;
    this.state = JdbcWritableBridge.readString(1, __dbResults);
    this.origin = JdbcWritableBridge.readString(2, __dbResults);
    this.destination_state = JdbcWritableBridge.readString(3, __dbResults);
    this.destination = JdbcWritableBridge.readString(4, __dbResults);
    this.sctg = JdbcWritableBridge.readInteger(5, __dbResults);
    this.commodity = JdbcWritableBridge.readString(6, __dbResults);
    this.value = JdbcWritableBridge.readInteger(7, __dbResults);
    this.tons = JdbcWritableBridge.readInteger(8, __dbResults);
    this.ton_miles = JdbcWritableBridge.readInteger(9, __dbResults);
    this.value_cv = JdbcWritableBridge.readDouble(10, __dbResults);
    this.tons_cv = JdbcWritableBridge.readDouble(11, __dbResults);
    this.ton_miles_cv = JdbcWritableBridge.readDouble(12, __dbResults);
  }
  public void loadLargeObjects(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void write(PreparedStatement __dbStmt) throws SQLException {
    write(__dbStmt, 0);
  }

  public int write(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(state, 1 + __off, 1, __dbStmt);
    JdbcWritableBridge.writeString(origin, 2 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(destination_state, 3 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(destination, 4 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeInteger(sctg, 5 + __off, -6, __dbStmt);
    JdbcWritableBridge.writeString(commodity, 6 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeInteger(value, 7 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeInteger(tons, 8 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeInteger(ton_miles, 9 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeDouble(value_cv, 10 + __off, 8, __dbStmt);
    JdbcWritableBridge.writeDouble(tons_cv, 11 + __off, 8, __dbStmt);
    JdbcWritableBridge.writeDouble(ton_miles_cv, 12 + __off, 8, __dbStmt);
    return 12;
  }
  public void readFields(DataInput __dataIn) throws IOException {
    if (__dataIn.readBoolean()) { 
        this.state = null;
    } else {
    this.state = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.origin = null;
    } else {
    this.origin = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.destination_state = null;
    } else {
    this.destination_state = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.destination = null;
    } else {
    this.destination = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.sctg = null;
    } else {
    this.sctg = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.commodity = null;
    } else {
    this.commodity = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.value = null;
    } else {
    this.value = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.tons = null;
    } else {
    this.tons = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.ton_miles = null;
    } else {
    this.ton_miles = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.value_cv = null;
    } else {
    this.value_cv = Double.valueOf(__dataIn.readDouble());
    }
    if (__dataIn.readBoolean()) { 
        this.tons_cv = null;
    } else {
    this.tons_cv = Double.valueOf(__dataIn.readDouble());
    }
    if (__dataIn.readBoolean()) { 
        this.ton_miles_cv = null;
    } else {
    this.ton_miles_cv = Double.valueOf(__dataIn.readDouble());
    }
  }
  public void write(DataOutput __dataOut) throws IOException {
    if (null == this.state) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, state);
    }
    if (null == this.origin) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, origin);
    }
    if (null == this.destination_state) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, destination_state);
    }
    if (null == this.destination) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, destination);
    }
    if (null == this.sctg) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.sctg);
    }
    if (null == this.commodity) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, commodity);
    }
    if (null == this.value) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.value);
    }
    if (null == this.tons) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.tons);
    }
    if (null == this.ton_miles) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.ton_miles);
    }
    if (null == this.value_cv) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeDouble(this.value_cv);
    }
    if (null == this.tons_cv) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeDouble(this.tons_cv);
    }
    if (null == this.ton_miles_cv) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeDouble(this.ton_miles_cv);
    }
  }
  private final DelimiterSet __outputDelimiters = new DelimiterSet((char) 44, (char) 10, (char) 0, (char) 0, false);
  public String toString() {
    return toString(__outputDelimiters, true);
  }
  public String toString(DelimiterSet delimiters) {
    return toString(delimiters, true);
  }
  public String toString(boolean useRecordDelim) {
    return toString(__outputDelimiters, useRecordDelim);
  }
  public String toString(DelimiterSet delimiters, boolean useRecordDelim) {
    StringBuilder __sb = new StringBuilder();
    char fieldDelim = delimiters.getFieldsTerminatedBy();
    __sb.append(FieldFormatter.escapeAndEnclose(state==null?"null":state, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(origin==null?"null":origin, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(destination_state==null?"null":destination_state, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(destination==null?"null":destination, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(sctg==null?"null":"" + sctg, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(commodity==null?"null":commodity, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(value==null?"null":"" + value, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(tons==null?"null":"" + tons, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ton_miles==null?"null":"" + ton_miles, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(value_cv==null?"null":"" + value_cv, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(tons_cv==null?"null":"" + tons_cv, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(ton_miles_cv==null?"null":"" + ton_miles_cv, delimiters));
    if (useRecordDelim) {
      __sb.append(delimiters.getLinesTerminatedBy());
    }
    return __sb.toString();
  }
  private final DelimiterSet __inputDelimiters = new DelimiterSet((char) 44, (char) 10, (char) 0, (char) 0, false);
  private RecordParser __parser;
  public void parse(Text __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharSequence __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(byte [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(char [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(ByteBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  private void __loadFromFields(List<String> fields) {
    Iterator<String> __it = fields.listIterator();
    String __cur_str;
    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.state = null; } else {
      this.state = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.origin = null; } else {
      this.origin = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.destination_state = null; } else {
      this.destination_state = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.destination = null; } else {
      this.destination = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.sctg = null; } else {
      this.sctg = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.commodity = null; } else {
      this.commodity = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.value = null; } else {
      this.value = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.tons = null; } else {
      this.tons = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ton_miles = null; } else {
      this.ton_miles = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.value_cv = null; } else {
      this.value_cv = Double.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.tons_cv = null; } else {
      this.tons_cv = Double.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.ton_miles_cv = null; } else {
      this.ton_miles_cv = Double.valueOf(__cur_str);
    }

  }

  public Object clone() throws CloneNotSupportedException {
    commodity o = (commodity) super.clone();
    return o;
  }

  public Map<String, Object> getFieldMap() {
    Map<String, Object> __sqoop$field_map = new TreeMap<String, Object>();
    __sqoop$field_map.put("state", this.state);
    __sqoop$field_map.put("origin", this.origin);
    __sqoop$field_map.put("destination_state", this.destination_state);
    __sqoop$field_map.put("destination", this.destination);
    __sqoop$field_map.put("sctg", this.sctg);
    __sqoop$field_map.put("commodity", this.commodity);
    __sqoop$field_map.put("value", this.value);
    __sqoop$field_map.put("tons", this.tons);
    __sqoop$field_map.put("ton_miles", this.ton_miles);
    __sqoop$field_map.put("value_cv", this.value_cv);
    __sqoop$field_map.put("tons_cv", this.tons_cv);
    __sqoop$field_map.put("ton_miles_cv", this.ton_miles_cv);
    return __sqoop$field_map;
  }

  public void setField(String __fieldName, Object __fieldVal) {
    if ("state".equals(__fieldName)) {
      this.state = (String) __fieldVal;
    }
    else    if ("origin".equals(__fieldName)) {
      this.origin = (String) __fieldVal;
    }
    else    if ("destination_state".equals(__fieldName)) {
      this.destination_state = (String) __fieldVal;
    }
    else    if ("destination".equals(__fieldName)) {
      this.destination = (String) __fieldVal;
    }
    else    if ("sctg".equals(__fieldName)) {
      this.sctg = (Integer) __fieldVal;
    }
    else    if ("commodity".equals(__fieldName)) {
      this.commodity = (String) __fieldVal;
    }
    else    if ("value".equals(__fieldName)) {
      this.value = (Integer) __fieldVal;
    }
    else    if ("tons".equals(__fieldName)) {
      this.tons = (Integer) __fieldVal;
    }
    else    if ("ton_miles".equals(__fieldName)) {
      this.ton_miles = (Integer) __fieldVal;
    }
    else    if ("value_cv".equals(__fieldName)) {
      this.value_cv = (Double) __fieldVal;
    }
    else    if ("tons_cv".equals(__fieldName)) {
      this.tons_cv = (Double) __fieldVal;
    }
    else    if ("ton_miles_cv".equals(__fieldName)) {
      this.ton_miles_cv = (Double) __fieldVal;
    }
    else {
      throw new RuntimeException("No such field: " + __fieldName);
    }
  }
}
