from matplotlib import pyplot as plt
from numpy import linspace,pi,sin,cos


if __name__ == "__main__":
    plt.figure(1)
    plt.subplot(211)
    x = linspace(0,2*pi,100)
    y = sin(x)
    z = cos(x)
    plt.plot(x,y,'-b',x,z,'r^')
    plt.xlabel("radians")
    plt.ylabel("value",fontsize=24)
    plt.axis([0,1.5*pi,-1,1])
    plt.annotate("some point",xy=(pi,0),xytext=(pi+0.5,0.5))
    plt.title("sin and cos")
    
    plt.subplot(212)
    plt.plot(x,1/x,"gs")
    plt.axis([0,10,0,10])
    plt.xlabel("x")
    plt.ylabel("y")
    plt.title("inverse of x")
    
    plt.show()