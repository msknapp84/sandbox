REGISTER /hadoop-annotations-2.0.0-cdh4.5.0.jar
REGISTER /hadoop-annotations.jar
REGISTER /hadoop-auth-2.0.0-cdh4.5.0.jar
REGISTER /hadoop-auth.jar
REGISTER /hadoop-common-2.0.0-cdh4.5.0.jar
REGISTER /hadoop-common-2.0.0-cdh4.5.0-tests.jar
REGISTER /hadoop-common.jar
REGISTER /original-parquet-avro-1.2.5-cdh4.5.0.jar
REGISTER /original-parquet-column-1.2.5-cdh4.5.0.jar
REGISTER /original-parquet-format-1.0.0-cdh4.5.0.jar
REGISTER /original-parquet-hadoop-1.2.5-cdh4.5.0.jar
REGISTER /original-parquet-pig-1.2.5-cdh4.5.0.jar
REGISTER /original-parquet-pig-bundle-1.2.5-cdh4.5.0.jar
REGISTER /original-parquet-thrift-1.2.5-cdh4.5.0.jar
REGISTER /parquet-avro-1.2.5-cdh4.5.0.jar
REGISTER /parquet-cascading-1.2.5-cdh4.5.0.jar
REGISTER /parquet-column-1.2.5-cdh4.5.0.jar
REGISTER /parquet-common-1.2.5-cdh4.5.0.jar
REGISTER /parquet-encoding-1.2.5-cdh4.5.0.jar
REGISTER /parquet-format-1.0.0-cdh4.5.0.jar
REGISTER /parquet-generator-1.2.5-cdh4.5.0.jar
REGISTER /parquet-hadoop-1.2.5-cdh4.5.0.jar
REGISTER /parquet-hive-1.2.5-cdh4.5.0.jar
REGISTER /parquet-pig-1.2.5-cdh4.5.0.jar
REGISTER /parquet-pig-bundle-1.2.5-cdh4.5.0.jar
REGISTER /parquet-scrooge-1.2.5-cdh4.5.0.jar
REGISTER /parquet-test-hadoop2-1.2.5-cdh4.5.0.jar
REGISTER /parquet-thrift-1.2.5-cdh4.5.0.jar

REGISTER /usr/lib/pig/hdfs-io.jar
DEFINE InsuranceLoader knapp.hdfs.importer.pig.InsuranceLoader;
insurance = LOAD '/home/msknapp/data/insurance_cnty3.seq' USING InsuranceLoader();
STORE insurance INTO '/home/msknapp/data/foo';