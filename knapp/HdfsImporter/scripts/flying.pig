REGISTER /usr/lib/pig/piggybank.jar
DEFINE AvroStorage org.apache.pig.piggybank.storage.avro.AvroStorage;

insurance = LOAD '/home/msknapp/data/insurance_cnty2.avro' USING AvroStorage();
carrollInsurance = FILTER insurance BY name == 'CARROLL';
simple = FOREACH carrollInsurance GENERATE name,st,fips;
DUMP simple;
-- STORE insurance INTO '/home/msknapp/insurance_output';

