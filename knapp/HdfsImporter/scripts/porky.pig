REGISTER /usr/lib/hadoop/hadoop-annotations-2.0.0-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/hadoop-annotations.jar
REGISTER /usr/lib/hadoop/hadoop-auth-2.0.0-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/hadoop-auth.jar
REGISTER /usr/lib/hadoop/hadoop-common-2.0.0-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/hadoop-common-2.0.0-cdh4.5.0-tests.jar
REGISTER /usr/lib/hadoop/hadoop-common.jar
REGISTER /usr/lib/hadoop/original-parquet-avro-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/original-parquet-column-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/original-parquet-format-1.0.0-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/original-parquet-hadoop-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/original-parquet-pig-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/original-parquet-pig-bundle-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/original-parquet-thrift-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/parquet-avro-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/parquet-cascading-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/parquet-column-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/parquet-common-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/parquet-encoding-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/parquet-format-1.0.0-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/parquet-generator-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/parquet-hadoop-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/parquet-hive-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/parquet-pig-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/parquet-pig-bundle-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/parquet-scrooge-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/parquet-test-hadoop2-1.2.5-cdh4.5.0.jar
REGISTER /usr/lib/hadoop/parquet-thrift-1.2.5-cdh4.5.0.jar


REGISTER /usr/lib/pig/piggybank.jar
DEFINE AvroStorage org.apache.pig.piggybank.storage.avro.AvroStorage;

insurance = LOAD '/user/msknapp/county_insurance_pp.txt' AS (fips:int,st:chararray,stfips:int,name:chararray,a:int,b:int,c:int,d:int,e:int,f:int,g:int);
DUMP insurance;
-- insurance = LOAD '/home/msknapp/data/county_insurance_pp.txt' AS (fips:int,st:chararray,stfips:int,name:chararray,a:int,b:int,c:int,d:int,e:int,f:int,g:int);
-- STORE insurance INTO '/home/msknapp/insurance_output';


-- #B = FILTER A BY name == 'CARROLL';
/*
Find 'CARROLL' in the input file.
*/

-- FIPS,ST,ST_FIPS,NAME, National Service Life Insurance , Service-Disabled Veteran's Insurance , 
-- United States Government Life Insurance , Veterans' Reopened Insurance , 
-- Veterans' Special Life Insurance , Veterans' Group Life Insurance ,Total