package knapp.hdfs.importer.insurance;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;

import knapp.hdfs.importer.AvroFileCsvCallback;
import knapp.hdfs.importer.CSVUtil;
import knapp.hdfs.importer.CsvTransformer;
import knapp.hdfs.importer.Insurance;
import knapp.hdfs.importer.InsuranceWritable;
import knapp.hdfs.importer.SequenceFileCsvCallback;

import org.apache.avro.file.CodecFactory;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.compress.DefaultCodec;

public final class InsuranceIO {
	private InsuranceIO(){}

	public static class InsuranceCsvTransformer implements CsvTransformer<Insurance> {

		@Override
		public Insurance transform(String[] line) {
			return InsuranceWritable.parseInsurance(line);
		}

		@Override
		public Writable getKey(Insurance obj) {
			if (obj==null) {
				return new Text("");
			}
			return new Text(obj.getSt().toString());
		}
	}

	public static <T> void writeFileToHdfsAsAvro(String filePath, String hdfsDestination,boolean skipFirstLine)
			throws IOException {
		FileReader coreReader = null;
		try {
			File file = new File(filePath);
			coreReader = new FileReader(file);
			writeToHdfsAsAvro(coreReader, hdfsDestination,skipFirstLine);
		} finally {
			org.apache.commons.io.IOUtils.closeQuietly(coreReader);
		}
	}

	public static <T> void writeFileToLocalFsAsAvro(String filePath, String destination,boolean skipFirstLine)
			throws IOException {
		FileReader coreReader = null;
		try {
			File file = new File(filePath);
			coreReader = new FileReader(file);
			writeToLocalFsAsAvro(coreReader,destination,skipFirstLine);
		} finally {
			org.apache.commons.io.IOUtils.closeQuietly(coreReader);
		}
	}

	public static <T> void writeToLocalFsAsAvro(Reader coreReader, String destination,boolean skipFirstLine)
			throws IOException {
		OutputStream outputStream = null;
		try {
			outputStream = new FileOutputStream(destination);
			writeAsAvro(coreReader,outputStream,skipFirstLine);
		} finally {
			IOUtils.closeQuietly(outputStream);
		}
	}

	public static <T> void writeToHdfsAsAvro(Reader coreReader, String hdfsDestination,boolean skipFirstLine)
			throws IOException {
		OutputStream outputStream = null;
		try {
			Configuration conf = new Configuration();
			conf.addResource(new Path("/etc/hadoop/conf/core-site.xml"));
			conf.addResource(new Path("/etc/hadoop/conf/hdfs-site.xml"));
			conf.addResource(new Path("/etc/hadoop/conf/mapred-site.xml"));
			conf.addResource(new Path("/etc/hadoop/conf/yarn-site.xml"));
			FileSystem fs = FileSystem.get(conf);
			Path path = new Path(hdfsDestination);
			if (fs.createNewFile(path)) {
				outputStream = fs.create(path);
				writeAsAvro(coreReader,outputStream,skipFirstLine);
			} else {
				System.err.println("Failed to create the file!");
			}
//		} 
//		catch (URISyntaxException e) {
//			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(outputStream);
		}
	}

	public static <T> void writeAsAvro(Reader coreReader, OutputStream outputStream,boolean skipFirstLine)
			throws IOException {
		DataFileWriter<Insurance> dataFileWriter = null;
		try {
			DatumWriter<Insurance> datumWriter = new SpecificDatumWriter<Insurance>(
					Insurance.class);
			dataFileWriter = new DataFileWriter<Insurance>(
					datumWriter);
			dataFileWriter.setSyncInterval(1<<20);
			dataFileWriter.setCodec(CodecFactory.snappyCodec());
			dataFileWriter.create(Insurance.SCHEMA$, outputStream);
			InsuranceCsvTransformer transformer = new InsuranceCsvTransformer();
			AvroFileCsvCallback<Insurance> callback = new AvroFileCsvCallback<Insurance>(dataFileWriter, transformer);
			CSVUtil.processCsv(coreReader, callback,skipFirstLine);
		} finally {
			if (dataFileWriter!=null) {
				dataFileWriter.close();
			}
		}
	}

	public static void writeFileToHdfsAsSequenceFile(String filePath,
			String hdfsDestination,boolean skipFirstLine) throws IOException {
		FileReader coreReader = null;
		try {
			File file = new File(filePath);
			coreReader = new FileReader(file);
			writeToHdfsAsSequenceFile(coreReader, hdfsDestination,skipFirstLine);
		} finally {
			if (coreReader!=null) {
				coreReader.close();
			}
		}
	}

	public static void writeFileToLocalFsAsSequenceFile(String filePath,
			String destination,boolean skipFirstLine) throws IOException {
		FileReader coreReader = null;
		try {
			File file = new File(filePath);
			coreReader = new FileReader(file);
			writeToLocalFsAsSequenceFile(coreReader, destination, skipFirstLine);
		} finally {
			if (coreReader!=null) {
				coreReader.close();
			}
		}
	}

	public static <T> void writeToHdfsAsSequenceFile(Reader coreReader,
			String hdfsDestination,boolean skipFirstLine) throws IOException {
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);
		Path path = new Path(hdfsDestination);
		writeAsSequenceFile(coreReader, conf, fs, path, skipFirstLine);
	}

	public static <T> void writeToLocalFsAsSequenceFile(Reader coreReader,
			String destination,boolean skipFirstLine) throws IOException {
		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.getLocal(conf);
		Path path = new Path(destination);
		writeAsSequenceFile(coreReader, conf, fs, path, skipFirstLine);
	}

	public static <T> void writeAsSequenceFile(Reader coreReader,
			Configuration conf,FileSystem fs,Path path,boolean skipFirstLine) throws IOException {
		SequenceFile.Writer writer = null;
		try {
			writer = SequenceFile.createWriter(fs,conf,path,Text.class,InsuranceWritable.class,
					SequenceFile.CompressionType.BLOCK,
					new DefaultCodec());
			CsvTransformer<Insurance> transformer = new InsuranceCsvTransformer();
			SequenceFileCsvCallback<Insurance> callback = new SequenceFileCsvCallback<Insurance>(writer,transformer);
			CSVUtil.processCsv(coreReader, callback,skipFirstLine);
		} finally {
			if (writer!=null) {
				writer.close();
			}
			if (coreReader !=null) {
				coreReader.close();
			}
		}
	}
}