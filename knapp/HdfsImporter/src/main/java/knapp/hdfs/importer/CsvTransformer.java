package knapp.hdfs.importer;

import org.apache.hadoop.io.Writable;

public interface CsvTransformer<T> extends org.apache.commons.collections4.Transformer<String[],T>{
	Writable getKey(T obj);
}