package knapp.hdfs.importer;

import java.io.IOException;

import org.apache.avro.file.DataFileWriter;
import org.apache.log4j.Logger;

public class AvroFileCsvCallback<T> implements CsvCallback {
	private static final Logger logger = Logger.getLogger(AvroFileCsvCallback.class);
	
	private final CsvTransformer<T> transformer;
	private final DataFileWriter<T> dataFileWriter;
	
	public AvroFileCsvCallback(final DataFileWriter<T> dataFileWriter,final CsvTransformer<T> transformer) {
		this.transformer = transformer;
		this.dataFileWriter = dataFileWriter;
	}
	
	@Override
	public void handleLine(String[] line) {
		T obj = transformer.transform(line);
		if (obj!=null) {
			try {
				dataFileWriter.append(obj);
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}
}