package knapp.hdfs.importer;

public interface CsvCallback {
	void handleLine(String[] line);
}
