package knapp.hdfs.importer;

import java.io.IOException;
import java.io.Reader;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;

public final class CSVUtil {
	private static final Logger logger = Logger.getLogger(ImporterCommandLine.class);
	
	private CSVUtil(){}
	
	public static void processCsv(Reader coreReader,CsvCallback callback,boolean skipFirstLine) {
		CSVReader reader = null;
		try {
			reader = new CSVReader(coreReader);
			String[] line;
			boolean firstLine=true;
			while ((line = reader.readNext()) != null) {
				if (!firstLine || !skipFirstLine) {
					callback.handleLine(line);
				}
				if (firstLine) {
					firstLine=false;
				}
			}
		} catch (IOException e) {
			logger.error(e);
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}
}
