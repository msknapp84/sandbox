package knapp.hdfs.importer;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableUtils;

public class InsuranceWritable extends Insurance implements Writable {

	public static InsuranceWritable parseInsurance(String[] line) {
		InsuranceWritable insurance = new InsuranceWritable();
		if (!StringUtils.isEmpty(line[0])) {
			insurance.setFips(Integer.valueOf(line[0]));
			insurance.setSt(line[1]);
			insurance.setStFips(Integer.valueOf(line[2]));
			insurance.setName(line[3]);
			insurance.setNationalServiceLifeInsurance(line[4]);
			insurance.setServiceDisabledVeteransInsurance(line[5]);
			insurance.setUsGovtLifeInsurance(line[6]);
			insurance.setVeteransReopenedInsurance(line[7]);
			insurance.setVeteransSpecialLifeInsurance(line[8]);
			insurance.setVeteransGroupLifeInsurance(line[9]);
			insurance.setTotal(line[10]);
			return insurance;
		}
		return null;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.fips = in.readInt();
		this.st = WritableUtils.readString(in);
		this.stFips = in.readInt();
		this.name = WritableUtils.readString(in);
		this.nationalServiceLifeInsurance = WritableUtils.readString(in);
		this.serviceDisabledVeteransInsurance = WritableUtils.readString(in);
		this.usGovtLifeInsurance = WritableUtils.readString(in);
		this.veteransReopenedInsurance = WritableUtils.readString(in);
		this.veteransSpecialLifeInsurance = WritableUtils.readString(in);
		this.veteransGroupLifeInsurance = WritableUtils.readString(in);
		this.total = WritableUtils.readString(in);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(this.fips);
		WritableUtils.writeString(out, this.st.toString());
		out.writeInt(this.stFips);
		WritableUtils.writeString(out, this.name.toString());
		WritableUtils.writeString(out,
				this.nationalServiceLifeInsurance.toString());
		WritableUtils.writeString(out,
				this.serviceDisabledVeteransInsurance.toString());
		WritableUtils.writeString(out, this.usGovtLifeInsurance.toString());
		WritableUtils.writeString(out,
				this.veteransReopenedInsurance.toString());
		WritableUtils.writeString(out,
				this.veteransSpecialLifeInsurance.toString());
		WritableUtils.writeString(out,
				this.veteransGroupLifeInsurance.toString());
		WritableUtils.writeString(out, this.total.toString());
	}

}
