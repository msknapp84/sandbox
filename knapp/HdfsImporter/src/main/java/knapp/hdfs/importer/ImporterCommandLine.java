package knapp.hdfs.importer;

import java.io.IOException;

import knapp.hdfs.importer.insurance.InsuranceIO;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.log4j.Logger;

public class ImporterCommandLine {
	private static final Logger logger = Logger
			.getLogger(ImporterCommandLine.class);
	private String source;
	private String destination;
	private String inputFormat;
	private String outputFormat;
	private String type;
	private boolean hasHeaderLine;

	private ImporterCommandLine(String source, String destination,
			String inputFormat, String outputFormat, String type,
			boolean hasHeaderLine) {
		this.source = source;
		this.destination = destination;
		this.inputFormat = inputFormat;
		this.outputFormat = outputFormat;
		this.type = type;
		this.hasHeaderLine = hasHeaderLine;
	}

	public static void main(String[] args) {
		Options options = new Options();
		options.addOption(
				"s",
				true,
				"set the source of data, accepts system prefixes like file:, hdfs:, http:, etc.");
		options.addOption(
				"d",
				true,
				"set the destination of data, accepts system prefixes like file:, hdfs:, http:, etc.");
		options.addOption("i", true,
				"set the input format (csv, avro, seq,seqblock,seqrow, plain)");
		options.addOption("o", true,
				"set the output format (avro, seq,seqblock,seqrow,plain)");
		options.addOption("t", true,
				"type of object each record is (insurance, facilityPollution, etc.)");
		options.addOption("h", false, "has header line");
		try {
			PosixParser parser = new PosixParser();
			CommandLine commandLine = parser.parse(options, args);
			String source = commandLine.getOptionValue('s');
			String destination = commandLine.getOptionValue('d');
			String inputFormat = commandLine.getOptionValue('i');
			String outputFormat = commandLine.getOptionValue('o');
			String type = commandLine.getOptionValue('t');
			boolean hasHeaderLine = commandLine.hasOption('h');
			ImporterCommandLine runner = new ImporterCommandLine(source,
					destination, inputFormat, outputFormat, type, hasHeaderLine);
			runner.run();
		} catch (Exception e) {
			HelpFormatter helpFormatter = new HelpFormatter();
			String cmdLineSyntax = "imports a file into hdfs in a specific format";
			helpFormatter.printHelp(cmdLineSyntax, options);
			e.printStackTrace();
		}
	}

	private void run() throws IOException {
		String protocol = "file";
		if (destination.contains(":")) {
			protocol = destination.split(":")[0];
			destination = destination.split(":")[1];
		}
		if ("hdfs".equalsIgnoreCase(protocol)) {
			if ("avro".equals(outputFormat)) {
				InsuranceIO.writeFileToHdfsAsAvro(source, destination,
						hasHeaderLine);
			} else {
				InsuranceIO.writeFileToHdfsAsSequenceFile(source, destination,
						hasHeaderLine);
			}
		} else {
			if ("avro".equals(outputFormat)) {
				InsuranceIO.writeFileToLocalFsAsAvro(source, destination,
						hasHeaderLine);
			} else {
				InsuranceIO.writeFileToLocalFsAsSequenceFile(source,
						destination, hasHeaderLine);
			}
		}
	}
}