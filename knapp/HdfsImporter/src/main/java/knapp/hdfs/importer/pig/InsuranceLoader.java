package knapp.hdfs.importer.pig;

import java.io.IOException;
import java.util.Arrays;

import knapp.hdfs.importer.InsuranceWritable;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileRecordReader;
import org.apache.pig.FileInputLoadFunc;
import org.apache.pig.backend.hadoop.executionengine.mapReduceLayer.PigSplit;
import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;

public class InsuranceLoader extends FileInputLoadFunc {

	private SequenceFileRecordReader<Text, InsuranceWritable> reader;

	@Override
	public InputFormat getInputFormat() throws IOException {
		return new SequenceFileInputFormat<Text, InsuranceWritable>();
	}

	@Override
	public Tuple getNext() throws IOException {
		boolean next = false;
		try {
			reader.nextKeyValue();
		} catch (InterruptedException e) {
			throw new IOException(e);
		}
		if (!next) {
			return null;
		}
		InsuranceWritable value = reader.getCurrentValue();
		if (value == null) {
			return null;
		}
		// FIPS,ST,ST_FIPS,NAME, National Service Life Insurance , 
		// Service-Disabled Veteran's Insurance , United States Government Life Insurance , 
		// Veterans' Reopened Insurance , Veterans' Special Life Insurance , 
		// Veterans' Group Life Insurance ,Total
		return TupleFactory.getInstance().newTuple(Arrays.asList(value.getFips(),value.getSt(),
				value.getStFips(),value.getName(),value.getNationalServiceLifeInsurance(),
				value.getServiceDisabledVeteransInsurance(),value.getUsGovtLifeInsurance(),
				value.getVeteransReopenedInsurance(),value.getVeteransSpecialLifeInsurance(),
				value.getVeteransGroupLifeInsurance(),value.getTotal()));
	}

	@Override
	public void prepareToRead(RecordReader reader, PigSplit split)
			throws IOException {
		this.reader = (SequenceFileRecordReader<Text, InsuranceWritable>) reader;
	}

	@Override
	public void setLocation(String location, Job job) throws IOException {
		FileInputFormat.setInputPaths(job, location);
	}

}
