package knapp.hdfs.importer;

import java.io.IOException;

import org.apache.hadoop.io.SequenceFile;
import org.apache.log4j.Logger;

public class SequenceFileCsvCallback<T> implements CsvCallback {
	private static final Logger logger = Logger.getLogger(SequenceFileCsvCallback.class);
	private final SequenceFile.Writer writer;
	private final CsvTransformer<T> transformer;
	
	public SequenceFileCsvCallback(final SequenceFile.Writer writer,final CsvTransformer<T> transformer) {
		this.writer = writer;
		this.transformer = transformer;
	}
	
	@Override
	public void handleLine(String[] line) {
		T obj = transformer.transform(line);
		if (obj!=null) {
			try {
				writer.append(transformer.getKey(obj),obj);
			} catch (IOException e) {
				logger.error(e);
			}
		}
	}
}