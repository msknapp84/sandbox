#! /usr/bin/env bash

set -e

ACTION=$1
CONSUMER_KEY=$2
CONSUMER_SECRET=$3
ACCESS_TOKEN=$4
ACCESS_TOKEN_SECRET=$5

function usage() {
    echo "Starts or stops the near-real-time loading of Twitter's public sample data"
    echo "stream into a collection in Solr using Twitter application / developer"
    echo "credentials. The first time you start this process, you will either need to"
    echo "provide the credentials as parameters, or you will need to manually edit"
    echo "/etc/flume-ng/conf/flume.conf and restart Flume. Subsequent runs will allow"
    echo "credentials to be over-ridden from the command-line, otherwise the"
    echo "credetials in /etc/flume-ng/conf/flume.conf will be used / reused."
    echo ""
    echo "Usage:"
    echo "    nrt-tweets.sh stop"
    echo "    nrt-tweets.sh start [CONSUMER_KEY] [CONSUMER_SECRET] [ACCESS_TOKEN]\\"
    echo "             [ACCESS_TOKEN_SECRET]"
    echo "    nrt-tweets.sh start"
    echo ""
    exit 1
}

sudo mkdir -p /var/lib/cloudera-demovm

case "$ACTION" in
    "stop" )
        sudo service flume-ng-agent stop
    ;;
    "start" )
        if [ -e /etc/flume-ng/conf/flume.conf ] ; then
            FLUME_CONFIG_FILE_ALREADY_EXISTED='true'
        else
            sudo service flume-ng-agent start
        fi

        if [ ! -e /var/lib/cloudera-demovm/nrt-tweets-generate.done ]; then
            rm -rf /home/cloudera/nrt_tweets_configs
            solrctl instancedir --generate /home/cloudera/nrt_tweets_configs
            cp -f /usr/share/doc/search*/examples/solr-nrt/collection1/conf/schema.xml /home/cloudera/nrt_tweets_configs/conf/
            sudo touch /var/lib/cloudera-demovm/nrt-tweets-generate.done
        fi

        if [ ! -e /var/lib/cloudera-demovm/nrt-tweets-create-dir.done ]; then
            solrctl instancedir --create nrt_tweets /home/cloudera/nrt_tweets_configs
            sudo touch /var/lib/cloudera-demovm/nrt-tweets-create-dir.done
        fi

        if [ ! -e /var/lib/cloudera-demovm/nrt-tweets-create-collection.done ]; then
            solrctl collection --create nrt_tweets -s 1
            sudo touch /var/lib/cloudera-demovm/nrt-tweets-create-collection.done
        fi

        if [ ! -e /var/lib/cloudera-demovm/nrt-tweets-flume-config.done ]; then
            sudo rm -rf /etc/flume-ng/conf/nrt_tweets
            sudo cp -r /home/cloudera/nrt_tweets_configs /etc/flume-ng/conf/nrt_tweets

            sudo cp /usr/share/doc/search*/examples/solr-nrt/twitter-flume.conf /etc/flume-ng/conf/flume.conf
            sudo cp /usr/share/doc/search*/examples/solr-nrt/test-morphlines/tutorialReadAvroContainer.conf /etc/flume-ng/conf/morphline.conf
            sudo sed -i -e 's#collection1#nrt_tweets#' /etc/flume-ng/conf/morphline.conf

            sudo cp /etc/flume-ng/conf/flume-env.sh.template /etc/flume-ng/conf/flume-env.sh
            JAVA_OPTS='JAVA_OPTS="-Xmx500m -Dsolr.host=localhost.localdomain"'
            sudo bash -c "echo '${JAVA_OPTS}' >> /etc/flume-ng/conf/flume-env.sh"

            sudo touch /var/lib/cloudera-demovm/nrt-tweets-flume-config.done
        fi

        if [ -n "$CONSUMER_KEY" ] ; then
            if [ -z "$CONSUMER_SECRET" -o -z "$ACCESS_TOKEN" -o -z "$ACCESS_TOKEN_SECRET" ] ; then
                echo "Error: you have only entered partial credentials. You must provide all of them"
                echo "on the command-line, or you must enter none of them and manually ensure"
                echo "/etc/flume-ng/conf/flume.conf contains the correct credentials."
                echo ""
                usage
            fi
        fi

        if [ -z "$CONSUMER_KEY" ] ; then
            if [ "$FLUME_CONFIG_FILE_ALREADY_EXISTED" == 'true' ] ; then
                sudo service flume-ng-agent restart
                echo "Warning: Re-using previously entered Twitter credentials..."
            else
                sudo service flume-ng-agent stop
                echo "Error: You must enter Twitter credentials in Flume config..."
                echo "(/etc/flume-ng/conf/flume.conf)"
            fi
        else
            if [ "$FLUME_CONFIG_FILE_ALREADY_EXISTED" == 'true' ] ; then
                echo "Overriding previously entered Twitter credentials..."
            fi
            sudo sed -i -e "s#agent.sources.twitterSrc.consumerKey.*#agent.sources.twitterSrc.consumerKey = ${CONSUMER_KEY}#" /etc/flume-ng/conf/flume.conf
            sudo sed -i -e "s#agent.sources.twitterSrc.consumerSecret.*#agent.sources.twitterSrc.consumerSecret = ${CONSUMER_SECRET}#" /etc/flume-ng/conf/flume.conf
            sudo sed -i -e "s#agent.sources.twitterSrc.accessToken[^S].*#agent.sources.twitterSrc.accessToken = ${ACCESS_TOKEN}#" /etc/flume-ng/conf/flume.conf
            sudo sed -i -e "s#agent.sources.twitterSrc.accessTokenSecret.*#agent.sources.twitterSrc.accessTokenSecret = ${ACCESS_TOKEN_SECRET}#" /etc/flume-ng/conf/flume.conf
            sudo service flume-ng-agent restart
        fi
    ;;
    * )
        usage
    ;;
esac

