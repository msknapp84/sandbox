#!/usr/bin/env python
'''
The purpose is to pre-process CSV, removing the first line, 
removing quotes, replacing real commas with tabs.
That makes it usable by pig

'''
import sys
import csv
import os
import string
import optparse

def handle_command_line():
	parser = optparse.OptionParser(description="pre-process csv for hdfs and pig")
	parser.add_option("-k","--keep-header",action="store_true",dest="removeHeader",help='remove the first line of the csv',default=False)
	parser.add_option("-d","--delimiter",dest="delimiter",help='replace commas with this delimiter',default='	')
	parser.add_option("-f","--input-file-path",dest="inputFilePath",help="path to the input file")
	parser.add_option("-o","--output-file-path",dest="outputFilePath",help="path to the output file")
	(options,args) = parser.parse_args()
	instream=sys.stdin
	if options.inputFilePath != None:
		instream=open(options.inputFilePath)
	outstream=sys.stdout
	if options.outputFilePath != None:
		outstream=open(options.outputFilePath,'w')
	fix_text(instream,outstream,options.delimiter,not options.removeHeader)
	

def fix_text(instream=sys.stdin,outstream=sys.stdout,delimiter=';',removeHeader=True):
	firstLine=True
	csvreader=csv.reader(instream,delimiter=',',quotechar='"')
	for line in csvreader:
		if not firstLine or not removeHeader:
			fixedfields=[]
			for field in line:
				fixedfield=string.replace(field,'"','')
				fixedfield=string.replace(fixedfield,'$','')
				fixedfield=string.replace(fixedfield,',','')
				fixedfield=string.strip(fixedfield)
				if (fixedfield == '-'):
					fixedfield=""
				fixedfields.append(fixedfield)
			outstream.write(delimiter.join(fixedfields)+"\n")
		if firstLine:
			firstLine=False

#def test():
	#instream=open("/home/msknapp/data/FY09_insurance_Cnty_11-13-09_GDX.csv")
	#outstream=open("/home/msknapp/data/county_insurance_pp.txt")
	#fix_text(instream)
	
#test()

handle_command_line()