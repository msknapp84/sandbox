import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line
num_rows_a=5
num_columns_a=5
num_rows_b=5
num_columns_b=5

def mapper(record):
    if record[0]=='a':
        row=record[1]
        # the row should be copied to all columns of the solution
        for solution_column in range(num_columns_b):
            mr.emit_intermediate((row,solution_column),record)
    else:
        # it's from b
        column = record[2]
        for solution_row in range(num_rows_a):
            mr.emit_intermediate((solution_row,column), record);

def reducer(key, list_of_values):
    a_row=[0 for i in range(num_rows_a)]
    b_col=[0 for i in range(num_columns_b)]
    for v in list_of_values:
        if v[0]=='a':
            a_row[v[2]]=v[3]
        else:
            b_col[v[1]]=v[3]
    cell_value=0
    for i in range(num_columns_a):
        cell_value+=a_row[i]*b_col[i]
    mr.emit((key[0],key[1],cell_value))
        

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
