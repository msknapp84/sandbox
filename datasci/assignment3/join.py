import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    mr.emit_intermediate(record[1],record)

def reducer(key, list_of_lists):
    orders = getAllOfType("order",list_of_lists)
    lineItems = getAllOfType("line_item",list_of_lists)
    list_of_result_lists = []
    for order in orders:
        for lineItem in lineItems:
            singleResultList=[]
            for cell in order:
                singleResultList.append(cell)
            for cell in lineItem:
                singleResultList.append(cell)
            list_of_result_lists.append(singleResultList)
    for resultList in list_of_result_lists:
        mr.emit(resultList)
    
def getAllOfType(type,list_of_lists):
    r=[]
    for currentList in list_of_lists:
        if currentList[0] == type:
            r.append(currentList)
    return r

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
