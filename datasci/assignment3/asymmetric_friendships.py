import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    mr.emit_intermediate(record[0],("im_friendly_to",record[1]))
    mr.emit_intermediate(record[1],("is_friendly_to_me",record[0]))
    

def reducer(me, list_of_tuples):
    peopleImFriendlyTo=getAllNamesOfType(list_of_tuples,"im_friendly_to");
    peopleFriendlyToMe=getAllNamesOfType(list_of_tuples,"is_friendly_to_me");
    nonSymmetric=set()
    for p in peopleImFriendlyTo:
        if p not in peopleFriendlyToMe:
            nonSymmetric.add(p)
    for p in peopleFriendlyToMe:
        if p not in peopleImFriendlyTo:
            nonSymmetric.add(p)
    '''for p in peopleFriendlyToMe:
        if p in nonSymmetric:
            nonSymmetric.remove(p)'''
    for p in nonSymmetric:
        mr.emit((me,p))

def reducer2(me, list_of_tuples):
    peopleImFriendlyTo=getAllNamesOfType(list_of_tuples,"im_friendly_to");
    peopleFriendlyToMe=getAllNamesOfType(list_of_tuples,"is_friendly_to_me");
    for f in peopleImFriendlyTo:
        if f in peopleFriendlyToMe:
            mr.emit((me,f))

def reducer3(me, list_of_tuples):
    peopleImFriendlyTo=getAllNamesOfType(list_of_tuples,"im_friendly_to");
    peopleFriendlyToMe=getAllNamesOfType(list_of_tuples,"is_friendly_to_me");
    nonSymmetric=set()
    for p in peopleFriendlyToMe:
        nonSymmetric.add(p)
    for p in peopleImFriendlyTo:
        if p in nonSymmetric:
            nonSymmetric.remove(p)
    for p in nonSymmetric:
        mr.emit((me,p))
        
def getAllNamesOfType(list_of_tuples,type):
    r=[]
    for tuple in list_of_tuples:
        if type == tuple[0]:
            r.append(tuple[1])
    return r 

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
