import sys
import json

states = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
}


def loadScores(afinnfile):
    scores = {}  # initialize an empty dictionary
    for line in afinnfile:
        term, score = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
        scores[term] = int(score)  # Convert the score to an integer.
    return scores

def lines(fp):
    print str(len(fp.readlines()))
    
def average(lst):
    sm = 0
    for item in lst:
        sm+=item
    return float(sm)/float(len(lst))

def guessState(location):
    for token in location.split(","):
        token = token.strip().upper()
        ps = token.split()
        for st in ps:
            st = st.strip().upper()
            state = confirmState(st)
            if state is not None:
                return state
    return None

def confirmState(state):
    # find it in the dictionary.
    for abbr in states:
        if abbr == state or states[abbr].upper()==state:
            return abbr
    return None

def printSentiment(tweet_file, scores):
    stateSentiments = {}
    #i=0
    for line in tweet_file:
        #if i > 1000:
        #    break
        #i+=1
        sentiment = 0
        try:
            data = json.loads(line)
            text = data.get("text")
            if text is None:
                continue
            
            terms = text.split()
            for term in terms:
                if (term in scores):
                    score = scores[term]
                    sentiment += score
            if sentiment == 0:
                continue
            # let's see if we can possibly get the location
            location = None
            if "place" in data:
                # use another try
                try:
                    place = data["place"]
                    if place is not None:
                        # I am in luck.
                        if "country" in place:
                            country = place["country"]
                            if country != "United States":
                                # wrong country!
                                continue
                            # it's the right country, let's try getting the state
                            if "full_name" in place:
                                fullName = place["full_name"]
                                # split by comma, trim
                                location = guessState(fullName)
                                #stt = fullName.split(", ")[1].upper()
                                #location = confirmState(stt)
                                # I got it!
                except:
                    pass
            if location is None and "user" in data:
                # ok so the place field did not work...
                # it's a long shot but try getting the location from the user
                try:
                    user = data["user"]
                    if "location" in user:
                        loc = user["location"]
                        if loc is not None:
                            # try splitting
                            location = guessState(loc)
                            #stt = loc.split(", ")[1].upper()
                            #location = confirmState(stt)
                except:
                    pass
            if location is not None:
                # this is one in a million, I know a sentiment 
                # and a place.
                if location not in stateSentiments:
                    # add a default
                    stateSentiments[location]=[]
                stateSentiments[location].append(sentiment)
        except:
            pass
    # now that I have the dictionary, let's get averages and sort
    avg = []
    for state in stateSentiments:
        lst = stateSentiments[state]
        myaverage = average(lst)
        tpl = (state,myaverage)
        avg.append(tpl)
    # now sort by sentiment descending
    avg.sort(comparator)
    
    #for t in avg:
    #    print t[0], t[1]
    
    # now print first entry
    print avg[0][0]
    #, avg[0][1]
        
def comparator(x,y):
    # descending by second field, the sentiment
    return int(100*(y[1]-x[1]))

def main():
    afinnfile = open(sys.argv[1])
    tweet_file = open(sys.argv[2])
    if afinnfile is None:	
        afinnfile = open("AFINN-111.txt")
    scores = loadScores(afinnfile)
    printSentiment(tweet_file, scores)

if __name__ == '__main__':
    main()
