import sys
import json

def loadScores(afinnfile):
    scores = {}  # initialize an empty dictionary
    for line in afinnfile:
        term, score = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
        scores[term] = int(score)  # Convert the score to an integer.
    return scores

def lines(fp):
    print str(len(fp.readlines()))

def deriveScores(tweet_file, scores):
    altTerms={}
    #altTermFrequency={}
    #altTermDocumentFrequency={}
    #documentNumber=0
    for line in tweet_file:
        sentiment = 0
        
        try:
            data = json.loads(line)
            text = data.get("text")
            if text is not None:
                terms = text.split()
                for term in terms:
                    if (term in scores):
                        score = scores[term]
                        sentiment += score
                # now that I have the total sentiment score,
                # derive sentiment for other terms
                if sentiment != 0:
                    for term in terms:
                        if term not in scores:
                            # this is a term we should derive sentiment for...
                            if term not in altTerms:
                                # establish a default value
                                altTerms[term]=0
                            #if term not in altTermFrequency:
                            #    altTermFrequency.put(term,0)
                            #if term not in altTermDocumentFrequency:
                            #    altTermDocumentFrequency.put(term,{})
                            altTerms[term]=altTerms[term]+sentiment
                            #altTermFrequency.put(term,altTermFrequency.get(term)+1)
                            #altTermDocumentFrequency.get(term).put(documentNumber,1);
                        
        except:
            pass
        #documentNumber+=1
    # print alternative term scores.
    for altTerm in altTerms:
        print altTerm, altTerms.get(altTerm)

def main():
    afinnfile = open(sys.argv[1])
    tweet_file = open(sys.argv[2])
    if afinnfile is None:    
        afinnfile = open("AFINN-111.txt")
    scores = loadScores(afinnfile)
    deriveScores(tweet_file, scores)

if __name__ == '__main__':
    main()
