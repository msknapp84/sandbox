import sys
import json

def loadScores(afinnfile):
    scores = {}  # initialize an empty dictionary
    for line in afinnfile:
        term, score = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
        scores[term] = int(score)  # Convert the score to an integer.
    return scores

def lines(fp):
    print str(len(fp.readlines()))

def printSentiment(tweet_file, scores):
    for line in tweet_file:
        sentiment = 0
        try:
            data = json.loads(line)
            text = data.get("text")
            if text is not None:
                terms = text.split()
                for term in terms:
                    if (term in scores):
                        score = scores[term]
                        sentiment += score
        except:
            pass
        print sentiment

def main():
    afinnfile = open(sys.argv[1])
    tweet_file = open(sys.argv[2])
    if afinnfile is None:	
        afinnfile = open("AFINN-111.txt")
    scores = loadScores(afinnfile)
    printSentiment(tweet_file, scores)

if __name__ == '__main__':
    main()
