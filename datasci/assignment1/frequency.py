
import sys
import json

def main():
    tweet_file = open(sys.argv[1])
    frequency={}
    mysum=float(0)
    for line in tweet_file:
        try:
            tweet = json.loads(line)
            if 'text' in tweet:
                text = tweet['text']
                terms = text.split()
                for term in terms:
                    term=term.lower()
                    if term.startswith("#") or term.startswith("http://"):
                        continue
                    if term not in frequency:
                        frequency[term]=0
                    frequency[term]=frequency[term]+1
                    mysum+=1
        except:
            pass
    for term in frequency:
        f = (float(frequency[term])/mysum)
        print term, f

if __name__ == '__main__':
    main()