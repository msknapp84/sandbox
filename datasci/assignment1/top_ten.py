
import sys
import json

def main():
    tweet_file = open(sys.argv[1])
    frequency={}
    for line in tweet_file:
        try:
            tweet = json.loads(line)
            if "entities" not in tweet:
                continue
            entities = tweet["entities"]
            if "hashtags" not in entities:
                continue
            hashtags = entities["hashtags"]
            if len(hashtags) == 0:
                continue
            for hashtag in hashtags:
                ht=hashtag["text"]
                if ht not in frequency:
                    frequency[ht]=0
                frequency[ht]=frequency[ht]+1
        except:
            pass
    # convert to a list of tuples
    lst = []
    for hashtag in frequency:
        o = (hashtag, frequency[hashtag])
        lst.append(o)
    # order that list.
    lst.sort(comparator)
    trimmed=lst[0:10]
    for item in trimmed:
        print item[0], item[1]
    
def comparator(x,y):
    return y[1]-x[1]

if __name__ == '__main__':
    main()